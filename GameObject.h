#pragma once
#include <SFML/Graphics.hpp>
#include "MovementComponent.h"


class GameObject
{
protected:
	
	std::string name;
	sf::Vector2f position;
	sf::Sprite* objectSprite;

	sf::CircleShape* debugShape;
	sf::RectangleShape* headingLine;

	class World* worldInstance;

public:
	GameObject();
	GameObject(class World* worldInstance,sf::Texture* texture, sf::Vector2f position);
	virtual ~GameObject();

	int objectTag = 0;
	bool isPendingKill = false;

	virtual void Update(float deltaTime);

	void SetPosition(float x, float y);
	void SetPosition(sf::Vector2f position);

	sf::Vector2f GetPosition();

	sf::CircleShape* GetDebugShape();
	sf::RectangleShape* GetHeadingLine();
	sf::Sprite* GetObjectSprite();

	class World* GetWorldInstance();
};

