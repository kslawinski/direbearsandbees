#pragma once
#include "GameObject.h"
class DroneBee :
	public GameObject
{
public:
	DroneBee();
	DroneBee(class World* worldInstance, sf::Texture* texture, sf::Vector2f position);
	void Update(float deltaTime) override;
	~DroneBee();

private:
	class MovementComponent* movementComponent;
	class SteeringBehaviours* steeringBehaviours;

	class DireBeer* direBear; // pointer to dire bear

	float lifeTimer;
	float lifeTime = 30;
};

