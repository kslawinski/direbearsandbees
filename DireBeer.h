#pragma once
#include "GameObject.h"

class DireBeer :
	public GameObject
{
public:
	DireBeer();
	DireBeer(class World* worldInstance, sf::Texture* texture, sf::Vector2f position);
	~DireBeer();

	void Update(float deltaTime) override;

	void GiveDamage(float damage);
	float GetHealth();

	float GetHoney();

	MovementComponent* GetMovementComponent();

private:
	float health;
	float honey;

	class BeeHive* beeHive;

	MovementComponent* movementComponent;

	float gatherHoneyCounter;
	float gatherHoneyInterval = 2;
};

