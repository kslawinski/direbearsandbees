#pragma once
#include "GameObject.h"
class Bee :
	public GameObject
{
public:
	Bee();
	Bee(class World* worldInstance, sf::Texture* texture, sf::Vector2f position);
	~Bee();

	void Update(float deltaTime) override;

	void GiveDamage(float damage);
	void RegenerateHealth(float health);
	void Attack(class DireBeer* bear);

	MovementComponent* GetMovementComponent();
	class SteeringBehaviours* GetSteeringBehaviours();

private:

	float health;
	float maxHealth = 100;

	class MovementComponent* movementComponent;
	class SteeringBehaviours* steeringBehaviours;

	class DireBeer* direBear; // pointer to dire bear

	float dt; // delta time

	float healtRegenTimer = 0;
	float healthRegenInterval = 8;

	float atackTimer;
	float attakInterval = 3;
};

