#include "MovementComponent.h"
#include "GameObject.h"
#include <math.h>
#include <stdio.h>
#include <iostream>
#include "World.h" 
#include "Bee.h"
#include "Utills.h"

MovementComponent::MovementComponent() // default constructor
{

}

MovementComponent::MovementComponent(GameObject * ownerObject)
{
	this->ownerObject = ownerObject;

	velocity = sf::Vector2f(0, 0);

	maxSpeed = 200; //300
	maxRotationSpeed =2;
	maxForce =1500;
	mass = 1;
}

void MovementComponent::Update(float deltaTime)
{
	dt = deltaTime;

	// apply friction
	velocity.x *= 0.8f;
	velocity.y *= 0.8f;

	if (ownerObject)
	{
		if (sqrt((velocity.x*velocity.x) + (velocity.y*velocity.y)) > 0.0001f)
		{
			sf::Vector2f newPos = ownerObject->GetPosition() + velocity * deltaTime;

			ownerObject->SetPosition(newPos);
			heading = Normalize(velocity);

			RotateToPosition(Normalize(heading));
		}
	}
}


MovementComponent::~MovementComponent()
{
}

sf::Vector2f MovementComponent::GetForward()
{
	float currentRotationAngles = ownerObject->GetObjectSprite()->getRotation();

	float x = sinf((pi / 180)* currentRotationAngles + 90);
	float y = -cosf((pi / 180)*currentRotationAngles + 90);

	return sf::Vector2f(x,y);
}

void MovementComponent::Rotate(float value)
{
	// clamp rotation speed
	if (value > maxRotationSpeed * dt)
		value = maxRotationSpeed * dt;

	ownerObject->GetObjectSprite()->rotate((int)(value * dt * maxRotationSpeed));
}

sf::Vector2f MovementComponent::GetAgentPosition()
{
	return ownerObject->GetPosition();
}

// apply some movement force to current velocity
void MovementComponent::ApplyMovementForce(sf::Vector2f forceToAdd)
{

	sf::Vector2f movementForce =  TruncateVector(forceToAdd,maxForce);

	//Acceleration = Force/Mass
	sf::Vector2f acceleration;
	acceleration.x = movementForce.x / mass;
	acceleration.y = movementForce.y / mass;

	//std::cout << ownerObject->GetPosition().x << "  " << ownerObject->GetPosition().y << std::endl;

	velocity += (acceleration * dt);

	velocity = TruncateVector(velocity, maxSpeed);
}

// this is only used to apply explosion force to bear
void MovementComponent::AddVelocity(sf::Vector2f velocity2)
{
	sf::Vector2f movementForce = velocity2;

	//Acceleration = Force/Mass
	sf::Vector2f acceleration;
	acceleration.x = movementForce.x / mass;
	acceleration.y = movementForce.y / mass;

	this->velocity += (acceleration * dt);
}

void MovementComponent::RotateToPosition(sf::Vector2f pos)
{
	// SFML's rotate-function uses degrees going clockwise from 0 to <360
	double angle = atan2(pos.x, pos.y);
	angle *= 180 / 3.14159265;  // Convert to degrees
	angle *= -1;  // Flip

	ownerObject->GetObjectSprite()->setRotation(angle - 180);
}

// velocity getter
sf::Vector2f MovementComponent::getVelocity()
{
	return velocity;
}

float MovementComponent::GetMaxSpeed()
{
	return maxSpeed;
}

void MovementComponent::SetMaxSpeed(float speed)
{
	maxSpeed = speed;
}

// returns the current heading of the owned entity
sf::Vector2f MovementComponent::GetHeading()
{
	return Normalize(heading);
}

////////////////// BEAR MOVEMENT //////////////////////////
void MovementComponent::MoveRight(float amount)
{
	sf::Vector2f movementForce = TruncateVector(sf::Vector2f(amount * maxSpeed,0), maxForce);

	//Acceleration = Force/Mass
	sf::Vector2f acceleration;
	acceleration.x = movementForce.x / mass;
	acceleration.y = movementForce.y / mass;

	//std::cout << ownerObject->GetPosition().x << "  " << ownerObject->GetPosition().y << std::endl;

	velocity += (acceleration * dt);

	velocity = TruncateVector(velocity, maxSpeed);
}

void MovementComponent::MoveUp(float amount)
{
	sf::Vector2f movementForce = TruncateVector(sf::Vector2f(0, amount * maxSpeed), maxForce);

	//Acceleration = Force/Mass
	sf::Vector2f acceleration;
	acceleration.x = movementForce.x / mass;
	acceleration.y = movementForce.y / mass;

	//std::cout << ownerObject->GetPosition().x << "  " << ownerObject->GetPosition().y << std::endl;

	velocity += (acceleration * dt);

	velocity = TruncateVector(velocity, maxSpeed);
}
///////////////////////////////////////////////////////////