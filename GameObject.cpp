#include "GameObject.h"
#include "World.h" 
#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

GameObject::GameObject()
{
	debugShape = new sf::CircleShape(10.f);
	debugShape->setFillColor(sf::Color::Red);

	position = sf::Vector2f(0, 0);

	headingLine = new sf::RectangleShape(sf::Vector2f(30, 3));
	headingLine->setFillColor(sf::Color::Yellow);
}

GameObject::GameObject(World* worldInstance, sf::Texture* texture, sf::Vector2f position)
{
	this->worldInstance = worldInstance;

	objectSprite = new sf::Sprite;
	objectSprite->setTexture(*texture);
	objectSprite->setOrigin(sf::Vector2f(objectSprite->getLocalBounds().width / 2, objectSprite->getLocalBounds().height / 2)); 

	this->position = position;
	objectSprite->setPosition(position);

	debugShape = new sf::CircleShape(10.0f);
	debugShape->setOrigin(sf::Vector2f(10, 5));
	debugShape->setFillColor(sf::Color::Red);

	headingLine = new sf::RectangleShape(sf::Vector2f(30, 3));
	headingLine->setFillColor(sf::Color::Yellow);

	headingLine->rotate(objectSprite->getRotation());
}


GameObject::~GameObject()
{
	//std::cout << "GameObject destructor called!" << std::endl;
	//delete objectSprite;
	delete headingLine;
	delete debugShape;
}

void GameObject::Update(float deltaTime)
{
	//debugShape.setPosition(position);
	objectSprite->setPosition(position);
}

void GameObject::SetPosition(float x, float y)
{
}

void GameObject::SetPosition(sf::Vector2f position)
{
	if (position.x > SCREEN_WIDTH + 600)
	{
		position.x = -30;
	}
	if (position.x < -600)
	{
		position.x = SCREEN_WIDTH + 30;
	}
	if (position.y > SCREEN_HEIGHT + 600)
	{
		position.y =   - 30;
	}
	if (position.y < -600)
	{
		position.y = SCREEN_WIDTH + 30;
	}

	this->position = position;
}

sf::Vector2f GameObject::GetPosition()
{
	return position;
}

sf::CircleShape* GameObject::GetDebugShape()
{
	return debugShape;
}

sf::RectangleShape * GameObject::GetHeadingLine()
{
	return headingLine;
}

sf::Sprite* GameObject::GetObjectSprite()
{
	return objectSprite;
}

World * GameObject::GetWorldInstance()
{
	return worldInstance;
}
