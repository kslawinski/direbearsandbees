#pragma once
#include "GameObject.h"
class BeeHive : public GameObject
{
public:
	BeeHive();
	BeeHive(class World* worldInstance, sf::Texture* texture, sf::Vector2f position);
	~BeeHive();

	void Update(float deltaTime);
	float dt;

	void SpawnWave();
	void SpawnBee(int quantity);

	float TakeHoney(float);

	float waveSpawnTimer;
	float waveSpawnInterval = 1;
	int waveSpawnCount = 0;
	int beeSpawnedCount = 0;

	float spawnBeeTimer;
	float spawnBeeInerval = 10;

	bool spawnFinished = true;

private:
	sf::Texture beeTexture;
	sf::Texture droneBeeTexture;
	int maxBeeCount = 150;
	float honeyLevel;
	bool isAlerted;
};

