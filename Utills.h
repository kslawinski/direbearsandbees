#pragma once
#include <SFML/Graphics.hpp>
#ifndef Utills
#define Utills

float DotProduct(sf::Vector2f a, sf::Vector2f b);
 sf::Vector2f Normalize( sf::Vector2f source);
 double CalculateDistance(sf::Vector2f a, sf::Vector2f b);
 sf::Vector2f TruncateVector(sf::Vector2f vector, double magnitude);
 float VectorGetLenth(sf::Vector2f vector);
 sf::Vector2f RotateVectorToDirection(sf::Vector2f& vector, float  value);

#endif