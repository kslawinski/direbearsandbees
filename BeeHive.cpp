#include "BeeHive.h"
#include "World.h"
#include "Bee.h"
#include "DroneBee.h"
#include "SteeringBehaviours.h"

#include <stdlib.h>     /* srand, rand */
#include <time.h>


BeeHive::BeeHive()
{
}

BeeHive::BeeHive(World * worldInstance, sf::Texture * texture, sf::Vector2f position)
{
	GameObject::GameObject(worldInstance, texture, position);

	this->worldInstance = worldInstance;

	objectSprite = new sf::Sprite;
	objectSprite->setTexture(*texture);
	objectSprite->setOrigin(sf::Vector2f(objectSprite->getLocalBounds().width / 2, objectSprite->getLocalBounds().height / 2));

	objectTag = 3;

	this->position = position;
	objectSprite->setPosition(position);

	if (!beeTexture.loadFromFile("Sprites/bee32.png"))
	{
		std::cout << "Cant load bee texture" << std::endl;
		return;
	}

	if (!droneBeeTexture.loadFromFile("Sprites/DroneBee32.png"))
	{
		std::cout << "Cant load bee texture" << std::endl;
		return;
	}

	waveSpawnTimer = waveSpawnInterval;

	honeyLevel = 1000;

	isAlerted = false;

	int i, n;
	time_t t;
	/* Intializes random number generator */
	srand((unsigned)time(&t));

	//SpawnWave();

}


BeeHive::~BeeHive()
{
	std::cout << "Behave destructor called!" << std::endl;
}

void BeeHive::Update(float deltaTime)
{
	GameObject::Update(deltaTime);
	dt = deltaTime;

	waveSpawnTimer -= dt * 2;
	spawnBeeTimer -= dt;

	if (isAlerted)
	{
			isAlerted = false;
			SpawnWave();
	}
	else
	{
		// spawn bee
		if (beeSpawnedCount < maxBeeCount)
		{
			if (spawnBeeTimer < 0)
			{
				if (worldInstance->GetWorldObjects().size() < 100)
				{
					SpawnBee(2); // spawn bees

					if (beeSpawnedCount % 2 == 0)
					{
						spawnBeeTimer = spawnBeeInerval;
						// Spawn DRONE BEE
						sf::Vector2f spawnLocation = position + sf::Vector2f(rand() % 32 + 1, rand() % 32 + 1);
						worldInstance->Instantiate<DroneBee>(&droneBeeTexture, spawnLocation);
					}
				}
			}
		}
	}

	if (!spawnFinished && beeSpawnedCount < maxBeeCount)
	{
		if (waveSpawnTimer < 0)
		{
			waveSpawnTimer = waveSpawnInterval;
			beeSpawnedCount++;
			waveSpawnCount++;

			sf::Vector2f spawnLocation = position + sf::Vector2f(rand() % 32 + 1, rand() % 32 + 1);

			worldInstance->Instantiate<Bee>(&beeTexture, spawnLocation);

			if (waveSpawnCount >= 30)
			{
				waveSpawnCount = 0;
				spawnFinished = true;
			}
		}
	}
}

void BeeHive::SpawnWave()
{
	if (waveSpawnTimer < 0)
	{
		spawnFinished = false;
		waveSpawnTimer = 2;
	}
}

void BeeHive::SpawnBee(int quantity)
{
	spawnBeeTimer = spawnBeeInerval;

	for (int i = 0; i < quantity; i ++)
	{
		sf::Vector2f spawnLocation = position + sf::Vector2f(rand() % 32 + 1, rand() % 32 + 1); // spawn bee at beehive with some random displacement

		worldInstance->Instantiate<Bee>(&beeTexture, spawnLocation);
		beeSpawnedCount++;
	}
}

float BeeHive::TakeHoney(float amount)
{
	float honey;

	//std::cout << "TAKE HONEY BEEHIVE " << std::endl;

	if (amount < honeyLevel)
	{
		honey = amount;
		honeyLevel - amount;
	}
	else if (amount > honeyLevel && honeyLevel > 0)
	{
		honey = honeyLevel;
		honeyLevel - amount;
	}

	if(spawnFinished)
	isAlerted = true;

	return honey;
}
