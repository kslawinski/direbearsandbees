#include "DroneBee.h"
#include "SteeringBehaviours.h"
#include "World.h"
#include "Utills.h"
#include "DireBeer.h"

DroneBee::DroneBee() // default constructor
{
}

DroneBee::DroneBee(World * worldInstance, sf::Texture * texture, sf::Vector2f position) // destructor, pass pointer to world, texture for the object, and position at scene
{
	GameObject::GameObject(worldInstance, texture, position);

	this->worldInstance = worldInstance;

	movementComponent = new MovementComponent(this); // create movement component and pass it the component owner
	movementComponent->SetMaxSpeed(140);
	steeringBehaviours = new SteeringBehaviours(movementComponent, this); // create steering behaviours component and pass movement component and owner

	objectSprite = new sf::Sprite;
	objectSprite->setTexture(*texture);
	objectSprite->setOrigin(sf::Vector2f(objectSprite->getLocalBounds().width / 2, objectSprite->getLocalBounds().height / 2));
	objectSprite->setScale(sf::Vector2f(1.3, 1.3));

	debugShape->setFillColor(sf::Color::Blue);
	debugShape->setOrigin(sf::Vector2f(debugShape->getLocalBounds().width / 2, debugShape->getLocalBounds().height / 2));

	this->position = position;
	objectSprite->setPosition(position);

	lifeTimer = lifeTime;
}

void DroneBee::Update(float deltaTime)
{
	if (this->isPendingKill)
		return;
	GameObject::Update(deltaTime);

	sf::Vector2f movementForce;

	if (!direBear)
	{
		std::vector<GameObject*> worldObjects = worldInstance->GetWorldObjects();

		for (int i = 0; i < worldObjects.size(); i++)
		{
			if (worldObjects[i] == nullptr) { return; }

			if (worldObjects[i]->isPendingKill) { return; }

			if (worldObjects[i]->objectTag == 2)
			{
				direBear = dynamic_cast<DireBeer*>(worldObjects[i]);
				break;
			}
		}
	}
	else if (direBear)
	{
		if (direBear->isPendingKill)
		{
			direBear = nullptr;

			return;
		}

		float distanceToAvaider = CalculateDistance(this->GetPosition(), direBear->GetPosition());

		if (distanceToAvaider < 2)
		{
				sf::Vector2f explosionForce = sf::Vector2f(-movementComponent->getVelocity().x * 5000 * deltaTime, -movementComponent->getVelocity().y * 5000 * deltaTime);
				direBear->GetMovementComponent()->AddVelocity(explosionForce);
				direBear->GiveDamage(5);
				worldInstance->DestroyObject(this);
		}
		else
		{
			movementForce = steeringBehaviours->CalculateArive(direBear->GetPosition(), movementComponent, 3);
			
			lifeTimer -= deltaTime;

			if (lifeTimer < 0)
			{
				lifeTimer = lifeTime;
				//DIE
				worldInstance->DestroyObject(this);
			}
		}
	}

	if (movementComponent && steeringBehaviours && !isPendingKill)
	{
		//steeringBehaviours->Update(deltaTime);
		movementComponent->ApplyMovementForce(movementForce);
		movementComponent->Update(deltaTime);
	}
}


DroneBee::~DroneBee()
{
	std::cout << "DroneBee destructor called!" << std::endl;

	delete movementComponent;
	delete objectSprite;
}
