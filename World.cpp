#include "World.h"


// default constructor
World::World()
{
}

World::World(sf::Vector2i screenDimension)
{

}


World::~World()
{
}

std::vector<GameObject*> World::GetWorldObjects()
{
	if (worldObjects.size() > 0 && worldObjects[0] != nullptr && !worldObjects[0]->isPendingKill)
		return worldObjects;
	else
		return std::vector<GameObject*>();
}

bool World::DestroyObject(GameObject * gameObject)
{
	bool deleteSuccessed = false;

	for (int i = 0; i < worldObjects.size(); i++)
	{
		if (worldObjects[i] == nullptr) { continue; }
		if (worldObjects[i]->isPendingKill) { continue; }

		if (worldObjects[i] == gameObject)
		{
			worldObjects[i]->isPendingKill = true;
			deleteSuccessed = true;
			if (worldObjects[i]->isPendingKill)
			{
				delete worldObjects[i];
				worldObjects.erase(worldObjects.begin() + i);

			}
		}
	}

	return deleteSuccessed;
}

void World::Draw(sf::RenderWindow* window)
{
	if (!window) { return; }

	for (int i = 0; i < worldObjects.size(); i++)
	{
		if (worldObjects.size() >= i)
		{
			if (worldObjects[i] != nullptr)
			{
				window->draw(*worldObjects[i]->GetObjectSprite());
				if (debugMode)
				{
					window->draw(*worldObjects[i]->GetDebugShape());
					window->draw(*worldObjects[i]->GetHeadingLine());
				}
			}
		}
	}
}

void World::Update(float deltaTime)
{
	if (deltaTime == 0) { return; }

	for (int i = 0; i < worldObjects.size(); i++)
	{
		if (worldObjects[i] != nullptr)
		{
			if (worldObjects[i]->isPendingKill) { return; }
			worldObjects[i]->Update(deltaTime);
		}
	}
}

void World::SwitchMode(int mode)
{
	if(mode == 1)
	{
		debugMode = true;
	}
	else if (mode == 2)
	{
		debugMode = false;
	}
}


