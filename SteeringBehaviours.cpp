#include "SteeringBehaviours.h"
#include "Utills.h"
#include "MovementComponent.h"
#include "GameObject.h"
#include <math.h>
#include <stdio.h>
#include <iostream>
#include "World.h" 
#include "Bee.h"

#include <stdlib.h>     /* srand, rand */
#include <time.h>


#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

double pi = acos(-1);
float wanderAngle = 45;

#define AGENT_DETECTION_DISTANCE 60

SteeringBehaviours::SteeringBehaviours() // Default Constructor
{
}

SteeringBehaviours::SteeringBehaviours(MovementComponent* agentMovementComponent, GameObject* ownerObject) // Constructor
{
	this->agentMovementComponent = agentMovementComponent;
	ownerAgent = ownerObject;

	srand(static_cast <unsigned> (time(0))); // seed
}

SteeringBehaviours::~SteeringBehaviours()
{
}

/////////////////////////// ALLIGNMENT /////////////////////////////////
sf::Vector2f SteeringBehaviours::CalculateAlighnment(GameObject * agent)
{
	sf::Vector2f allignmentVector = sf::Vector2f(0, 0);
	int neighborCount = 0;

	auto worldObjects = agent->GetWorldInstance()->GetWorldObjects();

	for (int i = 0; i < worldObjects.size(); i++)
	{
		if (worldObjects[i] == nullptr) { return sf::Vector2f(0,0); }

		if (worldObjects[i] != agent && worldObjects[i]->objectTag == 1) // check if the object is a bee type
		{
			if (CalculateDistance(agent->GetPosition(), worldObjects[i]->GetPosition()) < AGENT_DETECTION_DISTANCE)
			{
				Bee* bee = static_cast<Bee*>(worldObjects[i]); // cast it to Bee
				if (bee != nullptr)
				{
					allignmentVector += bee->GetMovementComponent()->getVelocity();
					neighborCount++;
				}
			}
		}
	}

	if (neighborCount <= 0)
		return allignmentVector;

	allignmentVector.x /= neighborCount;
	allignmentVector.y /= neighborCount;


	Normalize(allignmentVector);

	return allignmentVector;
}
////////////////////////////////////////////////////////////////////////

/////////////////////////// COHESION ///////////////////////////////////
sf::Vector2f SteeringBehaviours::CalculateCohesion(GameObject * agent)
{
	sf::Vector2f cohesionVector = sf::Vector2f(0, 0);
	int neighborCount = 0;

	auto worldObjects = agent->GetWorldInstance()->GetWorldObjects();

	for (int i = 0; i < worldObjects.size(); i++)
	{
		if (worldObjects[i] == nullptr) { return sf::Vector2f(0, 0); }

		if (worldObjects[i] != agent && worldObjects[i]->objectTag == 1)
		{
			if (CalculateDistance(agent->GetPosition(), worldObjects[i]->GetPosition()) < AGENT_DETECTION_DISTANCE)
			{
				cohesionVector += worldObjects[i]->GetPosition();
				neighborCount++;
			}
		}
	}

	if (neighborCount <= 0)
		return cohesionVector;

	cohesionVector.x /= neighborCount;
	cohesionVector.y /= neighborCount;

	cohesionVector = sf::Vector2f(cohesionVector.x - agent->GetPosition().x, cohesionVector.y - agent->GetPosition().y);

	Normalize(cohesionVector);

	return cohesionVector;
}
////////////////////////////////////////////////////////////////////////

/////////////////////////// SEPARATION /////////////////////////////////
sf::Vector2f SteeringBehaviours::CalculateSeparation(GameObject * agent)
{
	sf::Vector2f separationVector = sf::Vector2f(0, 0);
	int neighborCount = 0;

	auto worldObjects = agent->GetWorldInstance()->GetWorldObjects();

	for (int i = 0; i < worldObjects.size(); i++)
	{
		if (worldObjects[i] == nullptr) { return sf::Vector2f(0, 0); }

		if (worldObjects[i] != agent && worldObjects[i]->objectTag == 1)
		{
			if (CalculateDistance(agent->GetPosition(), worldObjects[i]->GetPosition()) < AGENT_DETECTION_DISTANCE)
			{
				separationVector.x += worldObjects[i]->GetPosition().x - agent->GetPosition().x;
				separationVector.y += worldObjects[i]->GetPosition().y - agent->GetPosition().y;
				neighborCount++;
			}
		}
	}

	if (neighborCount <= 0)
		return separationVector;

	separationVector.x /= neighborCount;
	separationVector.y /= neighborCount;

	separationVector.x *= -1;
	separationVector.y *= -1;

	Normalize(separationVector);

	return separationVector;
}
////////////////////////////////////////////////////////////////////////

/////////////////////////// SEEK ///////////////////////////////////////
sf::Vector2f SteeringBehaviours::CalculateSeek(sf::Vector2f targetPos, MovementComponent* agentMovementComp)
{
	if (!agentMovementComponent) { return sf::Vector2f(0, 0); }

	sf::Vector2f toTarget = Normalize( targetPos - ownerAgent->GetPosition());

	sf::Vector2f desiredVelocity = sf::Vector2f( toTarget.x * agentMovementComp->GetMaxSpeed(), toTarget.y * agentMovementComp->GetMaxSpeed());

	return (desiredVelocity - agentMovementComp->getVelocity());
}
////////////////////////////////////////////////////////////////////////

/////////////////////////// FLEE ///////////////////////////////////////
sf::Vector2f SteeringBehaviours::CalcculateFlee(sf::Vector2f targetPos, MovementComponent* agentMovementComp)
{
	if (!agentMovementComponent) { return sf::Vector2f(0, 0); }

	sf::Vector2f desiredVelocity = sf::Vector2f((agentMovementComp->GetAgentPosition().x - targetPos.x) * agentMovementComp->GetMaxSpeed(),(agentMovementComp->GetAgentPosition().y - targetPos.y) * agentMovementComp->GetMaxSpeed());

	return desiredVelocity - agentMovementComp->getVelocity();
}
////////////////////////////////////////////////////////////////////////

/////////////////////////// ARRIVE /////////////////////////////////////
sf::Vector2f SteeringBehaviours::CalculateArive(sf::Vector2f targetPos, MovementComponent* agentMovementComp, int speedMode)
{
	if (!agentMovementComponent) { return sf::Vector2f(0, 0); }

	float distance = CalculateDistance(targetPos, agentMovementComp->GetAgentPosition());

	if (distance > 0)
	{
		const double DecelerationTweeker = 0.8f;

		double speed = distance / ((double)speedMode * DecelerationTweeker);

		speed = fmin(speed, agentMovementComp->GetMaxSpeed());

		sf::Vector2f toTarget = targetPos - ownerAgent->GetPosition();

		sf::Vector2f desiredVelocity = sf::Vector2f(toTarget.x * speed / distance, toTarget.y * speed / distance);


		return desiredVelocity - agentMovementComp->getVelocity();

	}

	return sf::Vector2f(0, 0);
}
////////////////////////////////////////////////////////////////////////

/////////////////////////// WANDER /////////////////////////////////////
sf::Vector2f SteeringBehaviours::CalculateWander(MovementComponent* agentMovementComp)
{
	if (!agentMovementComponent) { return sf::Vector2f(0, 0); }

	//first, add a small random vector to the target's position
	float randomX = -1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1 - -1)));

	float cyrcleDistance = 60;
	float cyrcleRadius = 100;

	sf::Vector2f cyrcleCenter = sf::Vector2f(0, -1);
	cyrcleCenter = TruncateVector(cyrcleCenter,cyrcleDistance );

	// Calculate the displacement force
	sf::Vector2f displacement = sf::Vector2f(0, -1);
	displacement = TruncateVector(displacement,cyrcleRadius);

	// Randomly change the vector direction
	// by making it change its current angle
	displacement = setAngle(displacement, wanderAngle);

	// Change wanderAngle
	wanderAngle += randomX * 0.1f;

	   // Finally calculate and return the wander force
	sf::Vector2f wanderForce = cyrcleCenter + displacement;

	////////////////// DEBUG MODE ///////////////////////////////
	double angle = atan2(wanderForce.x, wanderForce.y);
	angle *= 180 / 3.14159265;  // Convert to degrees
	angle *= -1;  // Flip
	ownerAgent->GetHeadingLine()->setPosition(ownerAgent->GetPosition());
	ownerAgent->GetHeadingLine()->setRotation(angle - -90);
	//////////////////////////////////////////////////////////////

	return wanderForce;
}
////////////////////////////////////////////////////////////////////////

/////////////////////// COMBINED FORCE /////////////////////////////////
sf::Vector2f SteeringBehaviours::CalculateForce()
{
		sf::Vector2f combinedForce = sf::Vector2f(0, 0);

		//ALLIGNMENT
		sf::Vector2f allignment = CalculateAlighnment(ownerAgent);
		combinedForce.x = combinedForce.x + (allignment.x * alignmentModifier);
		combinedForce.y = combinedForce.y + (allignment.y * alignmentModifier);

		//COHESION
		sf::Vector2f cohesion = CalculateCohesion(ownerAgent);
		combinedForce.x = combinedForce.x + (cohesion.x * cohesionModifier);
		combinedForce.y = combinedForce.y + (cohesion.y * cohesionModifier);

		//SEPARATION
		sf::Vector2f separation = CalculateSeparation(ownerAgent);
		combinedForce.x = combinedForce.x + (separation.x * separationModifier);
		combinedForce.y = combinedForce.y + (separation.y * separationModifier);

		//WANDER
		if (agentMovementComponent)
		{
			sf::Vector2f wander = CalculateWander(agentMovementComponent);
			combinedForce.x = combinedForce.x + (wander.x * wanderModifier);
			combinedForce.y = combinedForce.y + (wander.y * wanderModifier);
		}
		
		//FLEE
		if (fleeON)
		{
			sf::Vector2f flee = CalcculateFlee(fleeLocation, agentMovementComponent);
			combinedForce.x = combinedForce.x + (flee.x);
			combinedForce.y = combinedForce.y + (flee.y);
		}
		//SEEK
		else if (SeekON)
		{
			sf::Vector2f seek = CalculateSeek(seekLocation, agentMovementComponent);
			combinedForce.x = combinedForce.x + (seek.x  );
			combinedForce.y = combinedForce.y + (seek.y );
		}

		return combinedForce;
}
////////////////////////////////////////////////////////////////////////

// Rotate to direction
sf::Vector2f SteeringBehaviours::setAngle(sf::Vector2f vector, float value)
{
	sf::Vector2f rotatedVector;
	float len = VectorGetLenth(vector);
	rotatedVector.x = cos(value) * len;
	rotatedVector.y = sin(value) * len;

	return rotatedVector;
}