#include "Bee.h"
#include "World.h"
#include "SteeringBehaviours.h"
#include "MovementComponent.h"
#include "Utills.h"
#include "DireBeer.h"

Bee::Bee()
{
}

Bee::Bee(World * worldInstance, sf::Texture * texture, sf::Vector2f position)
{
	GameObject::GameObject(worldInstance, texture, position);

	this->worldInstance = worldInstance;
	//Movement Component Init
	movementComponent = new MovementComponent(this);
	steeringBehaviours = new SteeringBehaviours(movementComponent, this);

	// set sprite
	objectSprite = new sf::Sprite;
	objectSprite->setTexture(*texture);
	objectSprite->setOrigin(sf::Vector2f(objectSprite->getLocalBounds().width / 2, objectSprite->getLocalBounds().height / 2));

	debugShape->setFillColor(sf::Color::Blue);
	debugShape->setOrigin(sf::Vector2f(debugShape->getLocalBounds().width / 2, debugShape->getLocalBounds().height / 2));


	this->position = position;
	objectSprite->setPosition(position);

	health = maxHealth;

	objectTag = 1;

	direBear = nullptr;

	int i, n;
	time_t t;
	/* Intializes random number generator */
	srand((unsigned)time(&t));
}


Bee::~Bee()
{
	//std::cout << "Bee destructor called!" << std::endl;
	delete movementComponent;
	delete steeringBehaviours;
	delete objectSprite;
}

void Bee::Update(float deltaTime)
{
	if (this->isPendingKill)
		return;
	GameObject::Update(deltaTime);

	if (healtRegenTimer < 0)
	{
		if (health < 100)
		{
			healtRegenTimer = healthRegenInterval;
			RegenerateHealth(2);
		}
	}

	dt = deltaTime;

	if (!direBear) // if direBear not assigned find and assign it
	{
		std::vector<GameObject*> worldObjects = worldInstance->GetWorldObjects();

		for (int i = 0; i < worldObjects.size(); i++)
		{
			if (worldObjects[i] == nullptr) { return; }

			if (worldObjects[i]->isPendingKill) { return; }

			if (worldObjects[i]->objectTag == 2)
			{
				direBear = dynamic_cast<DireBeer*>(worldObjects[i]);
			}
		}
	}
	else if (direBear) // if direBear assigned
	{
		if (direBear->isPendingKill) // if object is under destruction
		{
			//clear pointer
			direBear = nullptr;
			// disable behaviours
			steeringBehaviours->SeekON = false;
			steeringBehaviours->fleeON = false;
			return;
		}

		atackTimer -= deltaTime;
		
		float distanceTodireBear = CalculateDistance(this->GetPosition(), direBear->GetPosition());
		
		if (distanceTodireBear < 80) // if bear close enough
		{

			if( health > 35) // Attack bear if healthy
			{
				steeringBehaviours->seekLocation = direBear->GetPosition();
				steeringBehaviours->SeekON = true;
			}
			else if (health < 30) // run away if low health
			{
				steeringBehaviours->SeekON = false;
				steeringBehaviours->fleeON = true;

			}
		}
		if (distanceTodireBear > 250) // bear too far
		{
			// disable behaviours
			steeringBehaviours->SeekON = false;
			steeringBehaviours->fleeON = false;;

			// decrease heath regeneration timer
			healtRegenTimer -= deltaTime;
		}

		if (distanceTodireBear < 1) // if bear close enough
		{
			if (atackTimer < 0) // if ready to attack
			{
				if (direBear == nullptr) { return; } // safety check
				// APPLY small damage to dire bear
				Attack(direBear);
				GiveDamage(rand() % 60 + 25); // damage bee
			}
		}
	}

	if (movementComponent && steeringBehaviours && !isPendingKill)
	{
		sf::Vector2f movementForce = steeringBehaviours->CalculateForce();

		movementComponent->ApplyMovementForce(movementForce); // Apply movement force
		movementComponent->Update(deltaTime); // Update movement
	}
}

void Bee::GiveDamage(float damage)
{
	if (health >= health - damage)
	{
		health -= damage;
		//std::cout << health << std::endl;
	}

	if (health < 30)
	{
		objectSprite->setColor(sf::Color::Red);
	}
	
	if( health <= 0)
	{
		//std::cout << "KILLL IT!" << std::endl;
		worldInstance->DestroyObject(this);

		return;
	}
}

void Bee::RegenerateHealth(float health)
{
	//std::cout << "REGEN HEALTH: " << this->health << std::endl;

	if (health + this->health < maxHealth)
	{
		this->health += health;
	}
	else if (this->health > maxHealth)
	{
		this->health = maxHealth;
	}

	if (this->health > 35)
	{
		objectSprite->setColor(sf::Color::White);
	}

}

void Bee::Attack(DireBeer* bear)
{
	if (bear)
	{
		atackTimer = attakInterval;
		direBear->GiveDamage(0.2);
	}
}

MovementComponent * Bee::GetMovementComponent()
{
	return movementComponent;
}

SteeringBehaviours * Bee::GetSteeringBehaviours()
{
	return steeringBehaviours;
}
