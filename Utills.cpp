#include "Utills.h"

float DotProduct(sf::Vector2f a, sf::Vector2f b)
{
	return (a.x * b.x) + (a.y * b.y);
}

sf::Vector2f Normalize( sf::Vector2f source)
{
		// divide a vector's components by the vector's lenth
		float length = sqrt((source.x * source.x) + (source.y * source.y));
		if (length != 0)
			return sf::Vector2f(source.x / length, source.y / length);
		else
			return source;
}

double CalculateDistance(sf::Vector2f a, sf::Vector2f b)
{
		sf::Vector2f magnitude = a - b;
		double distance;

		// a*a + b*b = c*c
		distance = sqrt((magnitude.x*magnitude.x) + (magnitude.y*magnitude.y));

		return distance;
}

sf::Vector2f TruncateVector(sf::Vector2f vector, double magnitude)
{
		sf::Vector2f returnMagnitude = Normalize(vector);

		returnMagnitude.x *= magnitude;
		returnMagnitude.y *= magnitude;


		return returnMagnitude;
}

float VectorGetLenth(sf::Vector2f vector)
{
	return sqrt((vector.x * vector.x) + (vector.y * vector.y));
}

sf::Vector2f RotateVectorToDirection(sf::Vector2f& vector, float value)
{

		float lenth = VectorGetLenth(vector);

		vector.x = cos(value) * lenth;
		vector.y = sin(value) * lenth;

		return vector;
}
