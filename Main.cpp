#include <iostream>
#include <SFML/Graphics.hpp>
#include "GameObject.h"
#include "World.h"
#include "BeeHive.h"
#include "DireBeer.h"
#include "SteeringBehaviours.h"

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

int main()
{
	sf::Vector2i screenDimension = sf::Vector2i(SCREEN_WIDTH, SCREEN_HEIGHT);

	World worldInstance = World(screenDimension);

	sf::RenderWindow window(sf::VideoMode(screenDimension.x, screenDimension.y), "Dire Bears And Bees");

	sf::Clock clock; // clock used to calculate deltaTime
	float deltaTime; // variable to store delta time

	/////////////////////////// LOAD RESOURCES //////////////////////////////////
	sf::Texture beeHiveTexture;
	if (!beeHiveTexture.loadFromFile("Sprites/betterbeehive128.png"))
		return -1;

	sf::Texture bearTexture;
	if (!bearTexture.loadFromFile("Sprites/betterbear128.png"))
		return -1;

	sf::Font UIFont;
	if (!UIFont.loadFromFile("Fonts/fofbb_reg.ttf"))
		return -1;
	/////////////////////////////////////////////////////////////////////////////////

	worldInstance.Instantiate<BeeHive>(&beeHiveTexture, sf::Vector2f(200,200)); // spawn beehive

	// SPAWN DIRE BEAR
	DireBeer* direBear = worldInstance.Instantiate<DireBeer>(&bearTexture, sf::Vector2f(500,600)); // spawn bear


	//////////////// UI SETUP //////////////////////////////////
	sf::Text healthLvl, honeyLvl, gameOver, instruction;

	float uIUpdateTimer; // variable for UI updading
	float uIUpdateInterval = 1; // UI refresh every one seccond

	uIUpdateTimer = uIUpdateInterval;

	//Health
	healthLvl.setPosition(sf::Vector2f(20, 20));
	healthLvl.setFont(UIFont);
	healthLvl.setFillColor(sf::Color::Red);
	healthLvl.setString("HEALTH: 100");

	//Honey
	honeyLvl.setPosition(sf::Vector2f(healthLvl.getGlobalBounds().width + 40, 20));
	honeyLvl.setFont(UIFont);
	honeyLvl.setFillColor(sf::Color::Yellow);
	honeyLvl.setString("HONEY: 0");

	//Game Over
	gameOver.setOrigin(sf::Vector2f(gameOver.getGlobalBounds().width / 2, gameOver.getGlobalBounds().height / 2));
	gameOver.setPosition(sf::Vector2f((float)(SCREEN_WIDTH / 2) - 200, (float)SCREEN_HEIGHT / 2));
	gameOver.setCharacterSize(72);
	gameOver.setFont(UIFont);
	gameOver.setFillColor(sf::Color::Red);
	gameOver.setString("GAME OVER");

	// INSTRUCTION TEXT
	instruction.setPosition(sf::Vector2f( 20,SCREEN_HEIGHT - 35));
	instruction.setFont(UIFont);
	instruction.setFillColor(sf::Color::Yellow);
	instruction.setCharacterSize(14);
	instruction.setString("W,S,A,D to move. Steal honey from beehive, PRESS 1 for debuging mode");
	////////////////////////////////////////////////////////////

	while (window.isOpen())
	{
		sf::Time timeElapsed = clock.restart(); // restart clock and save how much time has pased synced last frame

///////////////////////// INPUT /////////////////////////////////

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			if (direBear != nullptr)
			{
				direBear->GetMovementComponent()->MoveRight(1); // player controll bear
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			if (direBear != nullptr)
			{
				direBear->GetMovementComponent()->MoveRight(-1);
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			if (direBear != nullptr)
			{
				direBear->GetMovementComponent()->MoveUp(-1);
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			if (direBear != nullptr)
			{
				direBear->GetMovementComponent()->MoveUp(1);
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			worldInstance.SwitchMode(1);
			std::cout << "GAME MODE 1: " << std::endl;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			worldInstance.SwitchMode(2);
			std::cout << "GAME MODE 2: " << std::endl;
		}

	uIUpdateTimer -= timeElapsed.asSeconds();

	if (uIUpdateTimer < 0 && !direBear->isPendingKill)
	{
		uIUpdateTimer = uIUpdateInterval;
		healthLvl.setString("HEALTH: " + std::to_string((int)direBear->GetHealth()));
		honeyLvl.setString("HONEY: " + std::to_string((int)direBear->GetHoney()));
	}

	worldInstance.Update(timeElapsed.asSeconds()); // update world

	window.clear(); // clear screen
	worldInstance.Draw(&window);
	//DRAW UI
	window.draw(healthLvl);
	window.draw(honeyLvl);
	window.draw(instruction);

	if (direBear->isPendingKill) // if bear dies show game over
	{
		window.draw(gameOver);
	}
	window.display();

		sf::Event event;
		while (window.pollEvent(event)) // handle window events
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			{
				event.type = sf::Event::Closed;
			}

			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
		}
	}

	return 0;
}