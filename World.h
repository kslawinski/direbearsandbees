#pragma once
#include <iostream>
#include <vector>
#include "GameObject.h"

class World
{
private:
	std::vector<GameObject*> worldObjects;
	bool debugMode = false;
public:
	World();
	World(sf::Vector2i screenDimension);
	~World();

	std::vector<GameObject*> GetWorldObjects();

	template <class T> T* Instantiate();

	template <class T> T* Instantiate(sf::Texture* texture, sf::Vector2f position);

	bool DestroyObject(GameObject * gameObject);
	void Draw(sf::RenderWindow* window);
	void Update(float deltaTime);
	void SwitchMode(int mode);
};

template<class T>
inline T* World::Instantiate()
{
	T* t = new T();
	worldObjects.push_back(t);
	return t;
}

template<class T>
inline T * World::Instantiate(sf::Texture* texture, sf::Vector2f position)
{
	T* t = new T(this,texture, position);
	worldObjects.push_back(t);
	return t;
}


