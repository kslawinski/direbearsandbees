#include "DireBeer.h"
#include "World.h"
#include "Utills.h"
#include "BeeHive.h"


DireBeer::DireBeer()
{
}

DireBeer::DireBeer(World * worldInstance, sf::Texture * texture, sf::Vector2f position)
{
	GameObject::GameObject(worldInstance, texture, position);

	this->worldInstance = worldInstance;
	//Movement Component Init
	movementComponent = new MovementComponent(this);


	objectSprite = new sf::Sprite;
	objectSprite->setTexture(*texture);
	objectSprite->setOrigin(sf::Vector2f(objectSprite->getLocalBounds().width / 2, objectSprite->getLocalBounds().height / 2));

	this->position = position;
	objectSprite->setPosition(position);

	health = 100;

	objectTag = 2;

	gatherHoneyCounter = gatherHoneyInterval;
}


DireBeer::~DireBeer()
{
	delete movementComponent;
}

void DireBeer::Update(float deltaTime)
{
	GameObject::Update(deltaTime);

	if (movementComponent)
	{
		movementComponent->Update(deltaTime);
		//std::cout << "Bear Velocity: " << "X: " << movementComponent->getVelocity().x << " Y: " << movementComponent->getVelocity().y << std::endl;
	}

	if (!beeHive)
	{
		std::vector<GameObject*> worldObjects = worldInstance->GetWorldObjects();

		for (int i = 0; i < worldObjects.size(); i++)
		{
			if (worldObjects[i]->objectTag == 3)
			{
				beeHive = dynamic_cast<BeeHive*>(worldObjects[i]);
			}
		}
	}
	else if (beeHive)
	{
		gatherHoneyCounter -= deltaTime;

		if (gatherHoneyCounter < 0)
		{
			float distanceToBeeHive = CalculateDistance(this->GetPosition(), beeHive->GetPosition());

			if (distanceToBeeHive < 20) // if close to beehive
			{

				gatherHoneyCounter = gatherHoneyInterval;

				// add Honey
				honey += beeHive->TakeHoney(5.0f);
			}
		}
	}
}

void DireBeer::GiveDamage(float damage)
{
	if (health >= health - damage)
	{
		health -= damage;
		//std::cout << health << std::endl;
	}

	if (health < 30)
	{
		objectSprite->setColor(sf::Color::Red);
	}

	if (health <= 0)
	{
		std::cout << "Dire bear dies!" << std::endl;
		worldInstance->DestroyObject(this);

		return;
	}
}

float DireBeer::GetHealth()
{
	return health;
}


float DireBeer::GetHoney()
{
	return honey;
}

MovementComponent * DireBeer::GetMovementComponent()
{
	return movementComponent;
}
