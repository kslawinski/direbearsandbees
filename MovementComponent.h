#pragma once
#include <SFML/Graphics.hpp>

class MovementComponent
{
private:
	sf::Vector2f velocity;
	sf::Vector2f forward;
	sf::Vector2f heading;
	sf::Vector2f right;

	double mass;
	double maxSpeed;
	double maxForce;
	float currentRotation;
	double maxRotationSpeed;

	float dt;

	class GameObject* ownerObject;

	void RotateToPosition(sf::Vector2f pos);

public:
	MovementComponent();
	MovementComponent(GameObject* ownerObject);
	void Update(float deltaTime);
	~MovementComponent();

	sf::Vector2f GetForward();
	void Rotate(float value);

	sf::Vector2f GetAgentPosition();
	void ApplyMovementForce(sf::Vector2f forceToAdd);
	void AddVelocity(sf::Vector2f velocity);

	sf::Vector2f getVelocity();
	float GetMaxSpeed();
	void SetMaxSpeed(float speed);
	sf::Vector2f GetHeading();

	void MoveRight(float amount);
	void MoveUp(float amount);

	double pi = acos(-1);
};

