#pragma once
#include <SFML/Graphics.hpp>
class SteeringBehaviours
{
public:
	SteeringBehaviours();
	SteeringBehaviours(class MovementComponent* agentMovementComponent, class GameObject* ownerObject);
	float dt;
	~SteeringBehaviours();

	bool fleeON = false;
	bool SeekON = false;
	sf::Vector2f seekLocation;
	sf::Vector2f fleeLocation;

	sf::Vector2f CalculateForce();
	sf::Vector2f CalculateWander(MovementComponent* agentMovementComp);

	sf::Vector2f setAngle(sf::Vector2f vector, float value);

private:
	////////// Floaking modifiers /////////
	float cohesionModifier = 0.4f;
	float separationModifier = 0.4f;
	float alignmentModifier = 0.2f;
	float wanderModifier = 0.03f;
	///////////////////////////////////////
	float wanderAngle;

	float seekModifier = 0.4f;
	float ariveModifier = 1;

	class MovementComponent* agentMovementComponent;
	class GameObject* ownerAgent;

	sf::Vector2f CalculateAlighnment(class GameObject* agent);
	sf::Vector2f CalculateCohesion(class GameObject* agent);
	sf::Vector2f CalculateSeparation(class GameObject* agent);
	sf::Vector2f CalcculateFlee(sf::Vector2f targetPos, MovementComponent* agentMovementComp);

public:
	sf::Vector2f CalculateArive(sf::Vector2f targetPos, MovementComponent* agentMovementComp, int speedMode);
	sf::Vector2f CalculateSeek(sf::Vector2f targetPos, MovementComponent* agentMovementComp);

};

